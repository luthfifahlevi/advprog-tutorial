package id.ac.ui.cs.advprog.tutorial2.command.core.spell;
import java.util.ArrayList;

public class ChainSpell implements Spell {
    ArrayList<Spell> spells;
    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }

    @Override
    public void cast() {
        for (Spell spell: this.spells) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for (int x = this.spells.size() - 1; x >= 0; x--) {
            this.spells.get(x).cast();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
