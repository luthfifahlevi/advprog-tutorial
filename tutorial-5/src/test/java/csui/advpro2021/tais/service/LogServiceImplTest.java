package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cglib.core.CollectionUtils;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.*;

public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @Mock
    private MahasiswaServiceImpl mahasiswaService;

    @InjectMocks
    private LogServiceImpl logService;

    private Mahasiswa mahasiswa;

    private Log log;

    private Log log2;

    private LogSummary logSummary;

    private LogSummary logSummary2;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa(
                "1906192052",
                "Maung Meong",
                "maung@cs.ui.ac.id", "4",
                "081317691718"
        );

        log = new Log();
        log.setAwalKerja(LocalDateTime.parse("2021-03-31T00:00:00"));
        log.setAkhirKerja(LocalDateTime.parse("2021-03-31T04:00:00"));
        log.setMahasiswa(mahasiswa);
        log.setDeskripsi("Tutorial-5");

        log2 = new Log();
        log2.setAwalKerja(LocalDateTime.parse("2020-04-30T00:00:00"));
        log2.setAkhirKerja(LocalDateTime.parse("2020-04-30T04:00:00"));
        log2.setMahasiswa(mahasiswa);
        log2.setDeskripsi("Tutorial-6");

        logSummary = new LogSummary(
                log.getAwalKerja().getMonth().getValue(),
                log.getAwalKerja().getYear(),
                log.getJamKerja()
        );

        logSummary2 = new LogSummary(
                log2.getAwalKerja().getMonth().getValue(),
                log2.getAwalKerja().getYear(),
                log2.getJamKerja()
        );

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testServiceCreateLog() {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(logService.createLog(log, mahasiswa.getNpm())).thenReturn(log);
    }

    @Test
    public void testServicegetLog() {
        lenient().when(logService.getLog("0")).thenReturn(log);
        Log resultLog = logService.getLog("0");
        assertEquals(log, resultLog);
    }

    @Test
    public void testServiceUpdateLog() {
        logService.createLog(log, mahasiswa.getNpm());
        String currentLogDescription = log.getDeskripsi();
        log.setDeskripsi("Hai!");

        lenient().when(logService.getLog("0")).thenReturn(log);
        lenient().when(logService.updateLog(log, "0")).thenReturn(log);
        Log resultLog = logService.getLog("0");

        assertNotEquals(currentLogDescription, resultLog.getDeskripsi());
        assertEquals(log.getDeskripsi(), resultLog.getDeskripsi());
    }

    @Test
    public void testDeleteLog() {
        logService.createLog(log, mahasiswa.getNpm());
        logService.deleteLog("0");
        lenient().when(logService.getLog("0")).thenReturn(null);
        assertEquals(null, logService.getLog("0"));
    }

    @Test
    public void testLogSummaryWithMonth() {
        List<LogSummary> summaries = new LinkedList<>(Arrays.asList(logSummary, logSummary2));
        when(logRepository.getLogSummary(mahasiswa.getNpm())).thenReturn(summaries);
        Iterable<LogSummary> summariesResult = logService.getLogSummary(mahasiswa.getNpm(), "March", null);
        assertEquals(summaries, summariesResult);
    }

    @Test
    public void testLogSummaryWithYear() {
        List<LogSummary> summaries = new LinkedList<>(Arrays.asList(logSummary, logSummary2));
        when(logRepository.getLogSummary(mahasiswa.getNpm())).thenReturn(summaries);
        Iterable<LogSummary> summariesResult = logService.getLogSummary(mahasiswa.getNpm(), null, "2021");
        assertEquals(summaries, summariesResult);
    }

    @Test
    public void testGetMonthName() {
        assertEquals("MARCH", logService.getMonthName(log.getAwalKerja()));
    }

    @Test
    public void testGetLogByNpmMahasiswa() {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        Iterable<Log> logs = logRepository.findByMahasiswa(mahasiswa);
        Iterable<Log> result = logService.getLogByNpm(mahasiswa.getNpm(), null);
        assertEquals(logs, result);
    }

    @Test
    public void testGetLogByNpmMahasiswaWithMonth() {
        List<Log> logs = new LinkedList<Log>(Arrays.asList(log, log2));
        when(logRepository.findByMahasiswa(mahasiswa)).thenReturn(logs);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        Iterable<Log> result = logService.getLogByNpm(mahasiswa.getNpm(), "March");
        logs.remove(log2);
        assertEquals(logs, result);
    }

    @Test
    public void testLogSummaryByNpm() {
        List<LogSummary> summaries = Arrays.asList(logSummary, logSummary2);
        when(logRepository.getLogSummary(mahasiswa.getNpm())).thenReturn(summaries);
        Iterable<LogSummary> summariesResult = logService.getLogSummary(mahasiswa.getNpm(), null, null);
        assertEquals(summaries, summariesResult);
    }
}
