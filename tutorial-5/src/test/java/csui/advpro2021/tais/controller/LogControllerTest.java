package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Arrays;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    private Mahasiswa mahasiswa;

    private Log log;
    private Log log2;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
        log = new Log();
        log.setAwalKerja(LocalDateTime.parse("2021-03-31T00:00:00"));
        log.setAkhirKerja(LocalDateTime.parse("2021-03-31T04:00:00"));
        log.setIdLog(0);
        log.setMahasiswa(mahasiswa);
        log.setDeskripsi("Hai");
        log2 = new Log(
                "2021-03-31T00:00:00",
                "2021-03-31T04:00:00",
                "Hai"
        );

    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerCreateLog() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(logService.createLog(log, "1906192052")).thenReturn(log);
        mvc.perform(post("/logs/user/1906192052")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(log)));
//                .andExpect(status().isOk());
    }

    @Test
    public void testControllerGetLog() throws Exception {
        when(logService.getLog("0")).thenReturn(log);
    }

    @Test
    public void testControllerUpdateLog() throws Exception {
        when(logService.createLog(any(Log.class), eq("1906192052"))).thenReturn(log);
        log.setDeskripsi("Hallo");
        String idLog = String.valueOf(log.getIdLog());
        when(logService.updateLog(log, idLog)).thenReturn(log);

        mvc.perform(put("/logs/0")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(log)));
    }

    @Test
    public void testControllerGetLogSummary() throws Exception {
        Iterable<Log> logs = Arrays.asList(log);
        when(logService.getLogByNpm("1906192052", null)).thenReturn(logs);
        mvc.perform(get("/logs/user/1906192052").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idLog").value(0));
    }

    @Test
    public void testControllerGetLogsReportMahasiswa() throws Exception {
        Iterable<LogSummary> summaries = Arrays.asList(new LogSummary(3, 2021, 4));
        when(logService.getLogSummary("1906192052", null, null)).thenReturn(summaries);
        mvc.perform(get("/logs/user/summary/1906192052").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].bulan").value("March"))
                .andExpect(jsonPath("$[0].tahun").value("2021"));
    }
}
