package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.service.LogService;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/logs")
public class LogController {
    @Autowired
    private LogService logService;

    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLog(@PathVariable(value = "idLog") String idLog) {
        return ResponseEntity.ok(logService.getLog(idLog));
    }

    @PutMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@RequestBody Log log, @PathVariable(value = "idLog") String idLog) {
        return ResponseEntity.ok(logService.updateLog(log, idLog));
    }

    @DeleteMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "idLog") String idLog) {
        logService.deleteLog(idLog);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/user/{npm}", produces = {"application/json"})
    public ResponseEntity getLogByNpm(
            @PathVariable(value = "npm") String npm,
            @RequestParam(required = false) String month) {
        return ResponseEntity.ok(logService.getLogByNpm(npm, month));
    }

    @PostMapping(path = "/user/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createLog(@PathVariable(value = "npm") String npm, @RequestBody Log log) {
        return ResponseEntity.ok(logService.createLog(log, npm));
    }

    @GetMapping(path = "/user/summary/{npm}", produces = {"application/json"})
    public ResponseEntity getLogSummary(
            @PathVariable(value = "npm") String npm,
            @RequestParam(required = false) String month,
            @RequestParam(required = false) String year) {
        return ResponseEntity.ok(logService.getLogSummary(npm, month, year));
    }
}
