package csui.advpro2021.tais.repository;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogRepository extends JpaRepository<Log, String> {
    Log findByIdLog(long idLog);

    List<Log> findByMahasiswa(Mahasiswa mahasiswa);

    @Query("SELECT new csui.advpro2021.tais.model.LogSummary( " +
            "EXTRACT (MONTH from log.awalKerja) AS month, " +
            "EXTRACT (YEAR from log.awalKerja) AS year, " +
            "SUM(log.jamKerja))" +
            "FROM Log AS log " +
            "WHERE npm = :npm " +
            "GROUP BY 1, 2")
    List<LogSummary> getLogSummary(String npm);
}
