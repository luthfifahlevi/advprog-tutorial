package csui.advpro2021.tais.service;

import org.springframework.beans.factory.annotation.Autowired;
import csui.advpro2021.tais.repository.*;
import csui.advpro2021.tais.model.*;
import org.springframework.stereotype.Service;
import java.time.*;
import java.util.*;

@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaService mahasiswaService;

    @Override
    public Log createLog(Log log, String npm) {
        Mahasiswa asdos = mahasiswaService.getMahasiswaByNPM(npm);
        log.setMahasiswa(asdos);
        logRepository.save(log);
        return log;
    }

    @Override
    public Log getLog(String idLog) {
        return logRepository.findByIdLog(Long.parseLong(idLog));
    }

    @Override
    public Iterable<Log> getLogByNpm(String npm, String month) {
        Mahasiswa asdos = mahasiswaService.getMahasiswaByNPM(npm);
        List<Log> logs = logRepository.findByMahasiswa(asdos);
        List<Log> result = new LinkedList<Log>();
        if (month != null) {
            logs.forEach(log -> {
                if (month.equalsIgnoreCase(getMonthName(log.getAwalKerja()))) result.add(log);
            });
        }
        return result;
    }

    @Override
    public Iterable<LogSummary> getLogSummary(String npm, String month, String year) {
        List<LogSummary> summaryLogs = logRepository.getLogSummary(npm);
        List<LogSummary> removed = new ArrayList<LogSummary>();
        if (month != null) {
            summaryLogs.forEach(log -> {
                if (!month.equalsIgnoreCase(log.getBulan())) removed.add(log);
            });
        }
        if (year != null) {
            summaryLogs.forEach(log -> {
                if (Integer.parseInt(year) != log.getTahun()) removed.add(log);
            });
        }
        removed.forEach(log -> { summaryLogs.remove(log); });
        return summaryLogs;
    }

    protected String getMonthName(LocalDateTime dateTime) {
        Month bulan = dateTime.getMonth();
        return bulan.name();
    }

    @Override
    public Log updateLog(Log log, String idLog) {
        Log theLog = this.getLog(idLog);
        log.setMahasiswa(theLog.getMahasiswa());
        log.setIdLog(Long.parseLong(idLog));
        logRepository.save(log);
        return log;
    }

    @Override
    public void deleteLog(String idLog) {
        logRepository.deleteById(idLog);
    }
}