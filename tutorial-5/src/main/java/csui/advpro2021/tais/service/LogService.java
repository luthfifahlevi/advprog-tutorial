package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;

public interface LogService {

    Log createLog(Log log, String npm);

    Iterable<Log> getLogByNpm(String npm, String month);

    Log getLog(String id);

    Iterable<LogSummary> getLogSummary(String npm, String month, String year);

    Log updateLog(Log log, String idLog);

    void deleteLog(String idLog);
}