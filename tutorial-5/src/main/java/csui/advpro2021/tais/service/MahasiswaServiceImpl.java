package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.hibernate.Session;

@Service
public class MahasiswaServiceImpl implements MahasiswaService {
    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private MataKuliahService mataKuliahService;

    @Override
    public Mahasiswa asdosRegisterMatkul(String npm, String kodeMatkul) {
        Mahasiswa asdos = this.getMahasiswaByNPM(npm);
        MataKuliah matkul = mataKuliahService.getMataKuliah(kodeMatkul);
        asdos.setMataKuliah(matkul);
        mahasiswaRepository.save(asdos);
        return asdos;
    }

    @Override
    public Mahasiswa createMahasiswa(Mahasiswa mahasiswa) {
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public Iterable<Mahasiswa> getListMahasiswa() {
        return mahasiswaRepository.findAll();
    }

    @Override
    public Mahasiswa getMahasiswaByNPM(String npm) {
        return mahasiswaRepository.findByNpm(npm);
    }

    @Override
    public Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa) {
        Mahasiswa nowAsdos = this.getMahasiswaByNPM(npm);
        mahasiswa.setNpm(npm);
        mahasiswa.setMataKuliah(nowAsdos.getMataKuliah());
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public void deleteMahasiswaByNPM(String npm) {
        mahasiswaRepository.deleteById(npm);
    }
}
