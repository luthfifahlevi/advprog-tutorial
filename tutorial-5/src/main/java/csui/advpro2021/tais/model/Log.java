package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Entity
@Table(name="log")
@Data
@Getter
@NoArgsConstructor
public class Log {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    @Column(name = "idLog", updatable = false, nullable = false)
    private long idLog;

    @Column(name = "awal_kerja")
    private LocalDateTime awalKerja;

    @Column(name = "akhir_kerja")
    private LocalDateTime akhirKerja;

    @Column(name = "jam_kerja")
    private double jamKerja;

    @Column(columnDefinition = "TEXT")
    private String deskripsi;

    @ManyToOne
    @JoinColumn(name = "npm")
    private Mahasiswa mahasiswa;

    @JsonCreator
    public Log(
            @JsonProperty("awalKerja") String awalKerja,
            @JsonProperty("akhirKerja") String akhirKerja,
            @JsonProperty("deskripsi") String deskripsi
    ) {
        this.awalKerja = LocalDateTime.parse(awalKerja);
        this.akhirKerja = LocalDateTime.parse(akhirKerja);
        this.deskripsi = deskripsi;
        this.jamKerja = (this.awalKerja.until(this.akhirKerja, ChronoUnit.SECONDS))/3600.0;
    }
}
