package csui.advpro2021.tais.model;

import lombok.*;
import java.text.DateFormatSymbols;

@Getter
public class LogSummary {
    private double jamKerja;
    private double pembayaran;
    private String bulan;
    private int tahun;

    public LogSummary(int bulan, int tahun, double jamKerja) {
        this.bulan = new DateFormatSymbols().getMonths()[bulan - 1];
        this.tahun = tahun;
        this.jamKerja = jamKerja;
        this.pembayaran = jamKerja * 350;
    }
}
