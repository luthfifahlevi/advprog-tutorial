
## Requirements Tutorial-5

1. Menyediakan relasi Many-To-One Relationship pada 'Mahasiswa mendaftar Mata Kuliah'
2. Menyediakan relasi One-To-Many Relationship pada 'Mahasiswa membuat Log'
3. Membuat model 'Log' untuk mencatat log tiap mahasiswa 
4. Membuat model 'Log Summary' untuk mencatat log mahasiswa tiap bulannya