package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
public class DunnoTransformationTest {
    private Class<?> dunnoClass;

    @BeforeEach
    public void setup() throws Exception {
        dunnoClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.DunnoTransformation");
    }

    @Test
    public void testDunnoHasEncodeMethod() throws Exception {
        Method translate = dunnoClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testDunnoEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "sAFIRA AND i WENT TO A BLACKSMITH TO FORGE OUR SWORD";

        Spell result = new DunnoTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testDunnoEncodesCorrectlyWithCustomKey() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "sAFIRA AND i WENT TO A BLACKSMITH TO FORGE OUR SWORD";

        Spell result = new DunnoTransformation(1).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testDunnoHasDecodeMethod() throws Exception {
        Method translate = dunnoClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testDunnoDecodesCorrectly() throws Exception {
        String text = "sAFIRA AND i WENT TO A BLACKSMITH TO FORGE OUR SWORD";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new DunnoTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testDummyDecodesCorrectlyWithCustomKey() throws Exception {
        String text = "sAFIRA AND i WENT TO A BLACKSMITH TO FORGE OUR SWORD";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new DunnoTransformation(1).decode(spell);
        assertEquals(expected, result.getText());
    }
}
