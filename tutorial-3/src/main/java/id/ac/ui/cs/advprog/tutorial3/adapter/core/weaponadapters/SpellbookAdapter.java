package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean isLatest_isLargeSpell;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.isLatest_isLargeSpell=false;
    }

    @Override
    public String normalAttack() {
        this.isLatest_isLargeSpell=false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if(!this.isLatest_isLargeSpell){ //jika false
            this.isLatest_isLargeSpell = true;
            return spellbook.largeSpell();
        }
        else return "Latest spell is large, so can't cast this spell again!";
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }
}
