package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private LogRepository logRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private WeaponRepository weaponRepository;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        List<Spellbook> spellbooks = spellbookRepository.findAll();
        List<Bow> bows = bowRepository.findAll();

        for(Spellbook spellbook : spellbooks){
            if(weaponRepository.findByAlias(spellbook.getName()) == null) {
                weaponRepository.save(new SpellbookAdapter(spellbook));
            }
        }
        for(Bow bow: bows){
            if(weaponRepository.findByAlias(bow.getName()) == null) {
                weaponRepository.save(new BowAdapter(bow));
            }
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon aliasWeapon = weaponRepository.findByAlias(weaponName);
        Bow aliasBow = bowRepository.findByAlias(weaponName);
        Spellbook aliasSpellbook = spellbookRepository.findByAlias(weaponName);


        aliasWeapon = (aliasWeapon != null) ?
                aliasWeapon : (aliasSpellbook != null) ? new SpellbookAdapter(aliasSpellbook): new BowAdapter(aliasBow);

        String typeAttack;
        if (attackType == 0) typeAttack = "(normal attack) " + aliasWeapon.normalAttack();
        else typeAttack = "(charge attack) " + aliasWeapon.chargedAttack();

        String myLogAttack = String.format("%s - %s: %s!",
                aliasWeapon.getHolderName(),
                aliasWeapon.getName(),
                typeAttack);

        logRepository.addLog(myLogAttack);
        weaponRepository.save(aliasWeapon);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
