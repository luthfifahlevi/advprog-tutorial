package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class DunnoTransformation {
    private int key;

    public DunnoTransformation(int key){
        this.key = key;
    }

    public DunnoTransformation(){
        this(1);
    }

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? key-1 : key+1;
        int n = text.length();
        char[] res = new char[n];
        for(int i = 0; i < n; i++) {
            int currentAscii = text.charAt(i);
            if (!Character.isAlphabetic(currentAscii)) {
                //nothing
            }else if(currentAscii >= 97){
                currentAscii -= 32;
            }else if(currentAscii >= 65){
                currentAscii += 32;
            }
            int newAscii = currentAscii;
            res[i] = (char)newAscii;
        }

        return new Spell(new String(res), codex);
    }
}
