Muhammad Luthfi Fahlevi - 1906293215

#### Pertanyaan:
Jelaskan perbeadaan keduanya beserta keuntungan dan kekurangan masing-masing ***approch***!

####Jawab:

***Lazy instantiation*** bermaksud melakukan pembuatan objeknya hanya dilakukan ketika pertama kali dipanggil (digunakan).
Jika melihat pada tutorial-4, yaitu ketika pemanggilan `getInstance()`.
Instansiasi jenis ini memberikan *public* API untuk mendapatkan *instance*.

Seperti pengertiannya, tujuannya adalah menghindari pembuatan objek yang tidak perlu.
Tujuan tersebut sangat berguna ketika pembuatan objek berharga mahal.
Oleh karena itu ***lazy instantiation*** cocok digunakan jika:
- Inisialisasi variabel instance memakan waktu lama dan/atau menggunakan resource dalam jumlah besar.
- Ada kemungkinan variabel tidak akan digunakan.

Berikut keuntungan dan kekurangan dari ***lazy instantiation***:

**Keuntungan:**
- Ketika awal *run-time* lebih lancar
- Hanya melakukan pembuatan objek yang diperlukan, sehingga dapat menghemat penggunaan memori dan *overhead*.


**Kerugian:**

- Apabila sering dilakukan pemanggilan `getInstance()`, maka pengecekan keberadaan objek juga dilakukan berkali-kali.
- Penanganan kasus *multithreading* ketika pemanggilan `getInstance()` bersamaan, cukup sulit.
- *Time penalty* terkait dengan setiap akses variabel akibat `isNil()` *test*.


Adapun ***eager instantiation*** berkebalikan dengan ***lazy instantiation***.
Pembuatan objek dilakukan dari awal sehingga pemanggilan `getInstance()` akan langsung mengembalikan (`return`) objek tersebut.
***Eager instantiation*** selalu membuat *instance* saat *class* dimuat ke JVM.
Oleh karena itu, instansiasi jenis ini cocok jika:
- Program selalu membutuhkan sebuah *instance*
- Biaya pembuatan *instance* tidak terlalu mahal 

Berikut keuntungan dan kerugian dari ***eager instantiation***:

**Keuntungan:**

- Dapat menangani *multithreading* lebih mudah.
- Tidak ada masalah jika sering dilakukan pemanggilan `getInstance()`.

**Kerugian:**

- Dapat membuat objek yang tidak diperlukan, sehingga dapat boros.
- *Cost* awal *overhead* mahal jika *resource*-nya banyak.