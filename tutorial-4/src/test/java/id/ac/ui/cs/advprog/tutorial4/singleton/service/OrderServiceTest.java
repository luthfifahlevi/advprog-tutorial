package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class OrderServiceTest {
    private Class<?> orderServiceClass;

    @Mock
    OrderDrink orderDrink;

    @Mock
    OrderFood orderFood;

    @InjectMocks
    OrderServiceImpl orderService = new OrderServiceImpl();

    @BeforeEach
    public void setUp() throws Exception {
        orderServiceClass = Class.forName(OrderServiceImpl.class.getName());
        orderDrink = mock(OrderDrink.class);
        orderFood = mock(OrderFood.class);
    }

    @Test
    public void testOrderADrinkCorrect() {
        String drink = "Sirup";
        when(orderDrink.getDrink()).thenReturn(drink);
        doNothing().when(orderDrink).setDrink(isA(String.class));

        orderService.orderADrink(drink);
        assertEquals(drink, orderService.getDrink().getDrink());
    }

    @Test
    public void testOrderAFoodCorrect() {
        String food = "Cake";
        when(orderFood.getFood()).thenReturn(food);
        doNothing().when(orderFood).setFood(isA(String.class));

        orderService.orderAFood(food);
        assertEquals(food, orderService.getFood().getFood());
    }
}