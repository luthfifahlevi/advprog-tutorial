package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class OrderDrinkTest {
    private OrderDrink orderDrink;
    private OrderDrink orderTest1;
    private OrderDrink orderTest2;

    @BeforeEach
    public void setUp() throws Exception {
        orderDrink = OrderDrink.getInstance();
        orderTest1 = orderDrink.getInstance();
        orderTest2 = orderDrink.getInstance();
    }

    @Test
    public void testOrderDrinkReturnSingletonInstance() {
        assertNotNull(orderDrink);
    }

    @Test
    public void testOrderDrinkCreatedJustOnce() {
        assertThat(orderTest1).isEqualToComparingFieldByField(orderTest2);
    }

}
