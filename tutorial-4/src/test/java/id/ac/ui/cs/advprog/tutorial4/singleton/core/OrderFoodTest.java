package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class OrderFoodTest {
    private OrderFood orderFood;
    private OrderFood orderTest1;
    private OrderFood orderTest2;

    @BeforeEach
    public void setUp() throws Exception {
        orderFood = OrderFood.getInstance();
        orderTest1 = orderFood.getInstance();
        orderTest2 = orderFood.getInstance();
    }

    @Test
    public void testOrderFoodReturnSingletonInstance() {
        assertNotNull(orderFood);
    }

    @Test
    public void testOrderDrinkCreatedJustOnce() {
        assertThat(orderTest1).isEqualToComparingFieldByField(orderTest2);
    }
}
